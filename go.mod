module gitlab.com/brurberguptime/watcher

go 1.15

require (
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0
	gitlab.com/brurberg/log/v2 v2.0.4
)
