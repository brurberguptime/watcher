package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"gitlab.com/brurberg/log/v2"
)

type DBConnect struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) GetAllSites() (sites []string, err error) {
	rows, err := db.Conn.Query("SELECT hostname FROM sites;")
	if err != nil {
		return
	}
	for rows.Next() {
		var site string
		if log.CheckError("Fetching Sites", rows.Scan(&site), log.Warning) {
			continue
		}
		sites = append(sites, site)
	}
	return
}

func (db DB) AddSiteStatus(site string, isUp bool) (err error) {
	result, err := db.Conn.Exec("INSERT INTO up (hostname, isup) VALUES ($1, $2);", site, isUp)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}
