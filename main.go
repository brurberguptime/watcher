package main

import (
	"bytes"
	"os"
	"os/exec"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurberguptime/watcher/db"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

func main() {
	loadEnvVar()

	dbCon, dbErr := db.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
	if dbErr != nil {
		log.Fatal(dbErr)
	}

	ticker := time.NewTicker(1 * time.Minute)
	for {
		select {
		case <-ticker.C:
			err := checkUptime(db.DB{Conn: dbCon})
			if err != nil {
				log.Println(err)
			}
		}
	}
}

func checkUptime(db db.DB) (err error) {
	sites, err := db.GetAllSites()
	for _, site := range sites {
		isUp, err := testSite(site)
		if err != nil {
			log.Println(err)
			err = db.AddSiteStatus(site, false)
			if err != nil {
				log.Println(err)
			}
			continue
		}
		err = db.AddSiteStatus(site, isUp)
		if err != nil {
			log.Println(err)
		}
	}
	return
}

func testSite(site string) (isUp bool, err error) {
	cmd := exec.Command("curl", "-s", "-w", "%{http_code}", site, "-o", "/dev/null")
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	isUp = out.String() == "200"
	return
}
