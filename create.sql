DROP DATABASE IF EXISTS brurberguptime;
CREATE DATABASE brurberguptime;
\c brurberguptime;

CREATE TABLE Sites (
  Hostname TEXT NOT NULL,
  CreatedAt TIMESTAMPTZ DEFAULT Now(),
  PRIMARY KEY (Hostname)
);

CREATE TABLE Up (
  ID BIGSERIAL PRIMARY KEY,
  Hostname TEXT NOT NULL REFERENCES Sites(Hostname),
  isup BOOL DEFAULT FALSE,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);
